import React from 'react';
import './App.css';
import Home from './containers/Home';
import Hero from './components/Hero/Hero';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Contact from './containers/Contact/contact';
import About from './containers/About/About';
import Post from './containers/Post';
import NotFound from './components/NotFoundPage/NotFound';

function App() {
  return (

    <Router>
      <div className="App">
        <Hero />
        <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/contact"  component={Contact}/>
        <Route path="/post/:slug" component={Post} />
        <Route path="/about" component={About} />
        <Route component={NotFound} />   
        </Switch>  
      </div>
    </Router>
    
  );
}

export default App;
