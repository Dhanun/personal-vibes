import React from 'react';
import './about.css'
import { Link } from 'react-router-dom';

const About = () => (
    <div className = "card card-block justify-content-center" >
        <div className="cardHeader">
            <span>About Us</span>
        </div>
        <div className="profileImageContainer">
            <img src={require('../../blogPostImages/Dhanashri.JPG' )} alt="" />
        </div>
        <div className="cardBody">
            <p className="personalBio">My name is Dhanashri Nikam. I am a software developer specialization in Front end developement....:)</p>
        </div>
    </div>
);

export default About;